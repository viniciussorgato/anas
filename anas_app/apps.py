from django.apps import AppConfig


class AnasAppConfig(AppConfig):
    name = 'anas_app'
