from django.db import models

class Cliente(models.Model):
    codigo = models.IntegerField(unique=True)
    cpf = models.CharField(db_index=True, max_length=20)
    nome = models.CharField(db_index=True, max_length=200)
    endereco = models.CharField(max_length=200)

    def __str__(self):
        return ' - '.join([str(self.codigo), self.cpf, self.nome, self.endereco])

class Produto(models.Model):
    codigo = models.CharField(unique=True, max_length=100)
    descricao = models.CharField(max_length=200)
    valor = models.FloatField()

    def __str__(self):
        return ' - '.join([self.codigo, self.descricao, str(self.valor)])