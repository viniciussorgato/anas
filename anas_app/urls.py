from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('venda', views.venda, name='venda'),
    path('venda-imprimir', views.venda_imprimir, name='venda_imprimir'),
    path('condicional', views.condicional, name='condicional'),
    path('condicional-imprimir', views.condicional_imprimir, name='condicional_imprimir'),
    path('atualizar', views.atualizar_bd, name='atualizar'),
    path('clients', views.clients, name='clients'),
    path('products', views.products, name='products'),
]