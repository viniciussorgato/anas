import csv

from .models import Produto, Cliente

# ============================================================
# ProductsParser
# ============================================================
class ProductsParser():
    def __init__(self, csvfile, sufix):
        self.csvfile = csvfile
        self.products = []
        self.sufix = sufix

    def parse(self):
        reader = csv.reader(self.csvfile, delimiter=',')
        price_column_index = None
        for row in reader:
            if 'Venda' in row:
                price_column_index = row.index('Venda') - 2

            try:
                code = int(row[1])
                description = row[6]
                price = self.extract_product_price(row, price_column_index)
                if code and description:
                    scode = str(code) + self.sufix
                    self.products.append({'icode': code, 'code': scode, 'description': description, 'price': price})
            except:
                pass
        self.products.sort(key=lambda item:item['icode'], reverse=True)

    def extract_product_price(self, row, column_index):
        try:
            price = float(row[column_index].replace('.', '').replace(',', '.'))
        except:
            price = 0.0
        return price

# ============================================================
# ClientsParser
# ============================================================

class ClientsParser():
    def __init__(self, csvfile):
        self.csvfile = csvfile
        self.clients = []

    def parse(self):
        reader = csv.reader(self.csvfile, delimiter=',')
        while True:
            try:
                row = next(reader)
                if len(row) and row[0].find('Cliente:') == 0:
                    client = {'code': 0, 'name': '', 'address': '', 'district': '', 'city': '', 'cpf': ''}
                    
                    code_name = row[3].split(' - ')
                    client['code'] = int(code_name[0])
                    client['name'] = code_name[1]
                    
                    row = next(reader)
                    client['address'] = row[3]
                    
                    row = next(reader)
                    client['district'] = row[3]
                    
                    row = next(reader)
                    client['city'] = row[3]
                    
                    row = next(reader)
                    row = next(reader)
                    client['cpf'] = row[3]

                    self.clients.append(client)
                else:
                    continue
            
            except StopIteration:
                break
            except:
                pass
            
        self.clients.sort(key=lambda item: item['code'], reverse=True)
        self.adjust_adresses()

    def adjust_adresses(self):
        for client in self.clients:
            district = client.pop('district', False)
            if district:
                client['address'] += ' - ' + district

            city = client.pop('city', False)
            if city:
                client['address'] += ' - ' + city
            
            