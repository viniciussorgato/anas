from django.shortcuts import render
from django.http import JsonResponse

from .models import Produto, Cliente
from .parsers import ProductsParser, ClientsParser
from io import BytesIO, StringIO

from datetime import datetime
from dateutil.relativedelta import relativedelta
from math import ceil

def index(request):
    return render(request, 'index.html')

def venda(request):
    return render(request, 'venda.html')

def venda_imprimir(request):
    promissory_data = get_promissory_data(request)
    promissory_data['emission_date'] = None
    payment_data = get_payment_data(request)
    context = {
        'promissory': promissory_data,
        'payment': payment_data
    }
    return render(request, 'venda-imprimir.html', context)

def condicional(request):
    return render(request, 'condicional.html')

def condicional_imprimir(request):
    salesman_name = request.POST.get('saleman-name', '')
    promissory_data = get_promissory_data(request)
    products = get_products_data(request)
    total_products = calculate_total(products)
    context = {
        'salesman_name': salesman_name,
        'promissory': promissory_data,
        'products': products,
        'total_products': total_products
    }
    return render(request, 'condicional-imprimir.html', context)

def atualizar_bd(request):
    context = {}
    if request.method == 'POST':
        context['products'] = []
        products_file = request.FILES.get('products-trentin-file', False)
        if products_file:
            _products = update_products_db(products_file, 'A')
            context['products'] = _products
            
        products_file = request.FILES.get('products-yes-file', False)
        if products_file:
            _products = update_products_db(products_file, 'Y')
            context['products'] += _products

        clients_file = request.FILES.get('clients-file', False)
        if clients_file:
            _clients = update_clients_db(clients_file)
            context['clients'] = _clients
        return render(request, 'relatorio-atualizacao-bd.html', context)
    return render(request, 'atualizar-bd.html', context)

def clients(request):
    _clients = []
    code = request.GET.get('code', False)
    name = request.GET.get('name', False)
    if code:
        queryset = Cliente.objects.all().filter(codigo__startswith=code)
        _clients = list(queryset.values())
    elif name:
        queryset = Cliente.objects.all().filter(nome__startswith=name)
        _clients = list(queryset.values())
    
    data = {
        'clients' : _clients
    }
    return JsonResponse(data)

def products(request):
    _products = []
    code = request.GET.get('code', False)
    if code:
        queryset = Produto.objects.all().filter(codigo__startswith=code)
        _products = list(queryset.values())
    data = {
        'products': _products
    }
    return JsonResponse(data)

# ============================================================
# General functions
# ============================================================

def get_promissory_data(request):
    values = {}
    values['emission_date'] = extract_date(datetime.now().strftime('%d/%m/%Y'))
    values['due_date'] = extract_date(request.POST.get('promissory-due-date', ''))
    values['value'] = string_to_float(request.POST.get('promissory-value', ''))
    values['client_code'] = request.POST.get('promissory-client-code', '')
    values['client_name'] = request.POST.get('promissory-client-name', '')
    values['client_cpf'] = request.POST.get('promissory-client-cpf', '')
    values['client_address'] = request.POST.get('promissory-client-address', '')
    return values

def get_payment_data(request):
    try:
        n_instalment = int(request.POST.get('payment-instalments', '1'))
    except:
        n_instalment = 1
    first_due_date_str = request.POST.get('payment-first-due-date', '01/01/2050')
    first_due_date = datetime.strptime(first_due_date_str, '%d/%m/%Y')
    total_value = string_to_float(request.POST.get('payment-value', '0,00'))
    
    instalment_value = float(ceil(total_value/n_instalment))
    last_instalment = total_value - instalment_value * (n_instalment - 1)
    last_instalment = round(last_instalment, 2)

    instalments = []
    for i in range(0, n_instalment):
        due_date = first_due_date + relativedelta(months=i)
        instalment = {
            'order': i+1,
            'due_date': due_date.strftime('%d/%m/%Y'),
            'value': instalment_value if i != n_instalment-1 else last_instalment
        }
        instalments.append(instalment)

    if n_instalment < 12:
        for i in range(n_instalment, 12):
            instalments.append({'order': str(i+1), 'due_date': '', 'value': ''})

    payment = {
        'total' : total_value,
        'instalments' : instalments
    }
    return payment

def get_products_data(request):
    products = []
    codes = request.POST.getlist('product-code', '')
    descriptions = request.POST.getlist('product-description', '')
    values = request.POST.getlist('product-value', '')
    for i in range(0, len(codes)):
        code = codes[i]
        description = descriptions[i]
        value = values[i]
        if description and value:
            product = {'code': code, 'description': description, 'value': value}
            products.append(product)
    return products

def calculate_total(_products):
    total = 0
    for product in _products:
        total += string_to_float(product['value'])
    total = round(total, 2)
    return total

def string_to_float(s):
    if s:
        return float(s.replace(".", "").replace(",", "."))
    return 0.0

def extract_date(sdate):
    new_date = {"day": "", "month": "", "year": ""}
    try:
        splited = sdate.split("/")
        new_date["day"] = splited[0]
        new_date["month"] = splited[1]
        new_date["year"] = splited[2]
    except:
        pass    
    return new_date

def update_products_db(uploaded_file, sufix):
    sio_file = django_uploaded_file_to_stringio(uploaded_file)
    parser = ProductsParser(sio_file, sufix)
    parser.parse()
    added_products = []
    for product in parser.products:
        p, created = Produto.objects.update_or_create(
            codigo=product['code'],
            defaults={
                'descricao': product['description'],
                'valor': product['price']})
        if created:
            print(product)
            added_products.append(p)
        else:
            print("Done updating products")
            break
    return added_products

def update_clients_db(uploaded_file):
    sio_file = django_uploaded_file_to_stringio(uploaded_file)
    parser = ClientsParser(sio_file)
    parser.parse()
    added_clients = []
    for client in parser.clients:
        c, created = Cliente.objects.update_or_create(
            codigo=client['code'],
            defaults={
                'cpf': client['cpf'],
                'nome': client['name'],
                'endereco': client['address']})
        if created:
            print(client)
            added_clients.append(c)
        else:
            print("Done updating clients")
            break
    return added_clients

def django_uploaded_file_to_stringio(uploaded_file):
    byte_str = BytesIO(uploaded_file.read()).read()
    text_obj = byte_str.decode('latin-1')
    return StringIO(text_obj)
