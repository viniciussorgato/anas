$.jMaskGlobals.watchInputs = true;

function float_to_cents(value) {
    x = 100 * parseFloat(value);
    return Math.floor(x.toFixed(2));
}

$(document).ready(function(){
    $('.date').mask('00/00/0000');
    $('.cep').mask('00000-000');
    $('.instalments').mask('00');
    $('.cpf').mask('000.000.000-00', {reverse: true});
    $('.money').mask('000.000.000.000.000,00', {reverse: true});

    $('.float-to-money').each(function() {
        var textValue = $(this).text();
        if (textValue) {
            var value = float_to_cents(textValue);
            $(this).text(value).mask('000.000.000.000.000,00', {reverse: true});
        }
    });

    $(".valor-extenso").each(function() {
        var that = $(this);
        that.text(that.text().extenso(false));
    });

    $(".valor-extenso-moeda").each(function() {
        var that = $(this);
        var text = that.text().replace('.', ',')
        that.text(text.extenso(true));
    });

    $(".valor-extenso-data").each(function() {
        var that = $(this);
        if (that.text() === "01") {
            that.text("primeiro");
        }
        else {
            that.text(that.text().extenso(false));
        }
    });

    $(".n-month-to-str").each(function() {
        months = ["", "janeiro", "fevereiro", "março", "abril", "maio", "junho", "julho", "agosto", "setembro", "outubro", "novembro", "dezembro"];
        var that = $(this);
        var smonth = months[parseInt(that.text())];
        that.text(smonth);
    });    
});