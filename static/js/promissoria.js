$( "#promissory-client-code" ).autocomplete({
    minLength: 1,
    delay: 500,
    autoFocus: true,
	source: function (request, response)
    {
        $.ajax(
        {
            url: "/clients" ,
            dataType: "json",
            data:
            {
                code: request.term,
            },
            success: function (data)
            {
                for (i in data.clients) {
                    data.clients[i].label = data.clients[i].codigo;
                }
                response(data.clients);
            }
        });
    },
    select: function( event, ui ) {
        $('#promissory-client-name').val(ui.item.nome);
        $('#promissory-client-cpf').val(ui.item.cpf);
        $('#promissory-client-address').val(ui.item.endereco);
        $('#promissory-client-cpf').trigger('input');
    }
});

$( "#promissory-client-name" ).autocomplete({
    minLength: 3,
    delay: 500,
	source: function (request, response)
    {
        $.ajax(
        {
            url: "/clients" ,
            dataType: "json",
            data:
            {
                name: request.term,
            },
            success: function (data)
            {
                for (i in data.clients) {
                    data.clients[i].label = data.clients[i].nome;
                }
                response(data.clients);
            }
        });
    },
    select: function( event, ui ) {
        $('#promissory-client-code').val(ui.item.codigo);
        $('#promissory-client-cpf').val(ui.item.cpf);
        $('#promissory-client-address').val(ui.item.endereco);
        $('#promissory-client-cpf').trigger('input');
    }
});