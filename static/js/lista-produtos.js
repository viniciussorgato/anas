$("#add_row").click(function() {
     $('.form-row.product-row.d-none:first').removeClass('d-none');
});

$('input[name^="product-code"]').autocomplete({
    minLength: 1,
    delay: 500,
    autoFocus: true,
	source: function (request, response)
    {
        $.ajax(
        {
            url: "/products" ,
            dataType: "json",
            data:
            {
                code: request.term,
            },
            success: function (data)
            {
                for (i in data.products) {
                    data.products[i].label = data.products[i].codigo;
                }
                response(data.products);
            }
        });
    },
    select: function( event, ui ) {
        var iCode = $(this);
        var productRow = iCode.parent().parent();
        var iDescription = productRow.find('input[name^="product-description"]');
        var iValue = productRow.find('input[name^="product-value"]');

        iCode.val(ui.item.codigo);
        iDescription.val(ui.item.descricao);
        iValue.val(float_to_cents(ui.item.valor));
        iValue.trigger('input');
    }
});