function calculateProductsTotal() {
    var total = 0.0;
    $("input[name='product-value']" ).each(function(index, input) {
        var value = parseFloat($(input).val().replace('.', '').replace(',', '.'));
        var isNumber = value == value;
        if (isNumber) {
            total += value;
        }
    });
    total = Math.round(total * 100);
    return total;
}

function update_promissory_value() {
    var total = calculateProductsTotal();
    $("#promissory-value").val(total);
    $('#promissory-value').trigger('input');
}

$("#promissory-due-date").focusout(update_promissory_value);
